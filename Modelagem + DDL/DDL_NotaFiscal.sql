DROP TABLE IF EXISTS Evento_NotaFiscal;
DROP TABLE IF EXISTS Produto_NotaFiscal;
DROP TABLE IF EXISTS Produto;
DROP TABLE IF EXISTS Nota_Fiscal;
DROP TABLE IF EXISTS Natureza_Operacao;
DROP TABLE IF EXISTS Presenca_Comprador;
DROP TABLE IF EXISTS Evento;
DROP TABLE IF EXISTS Tipo_Evento;
DROP TABLE IF EXISTS Emitente;
DROP TABLE IF EXISTS Destinatario;
DROP TABLE IF EXISTS NCM_SH_Tipo;
DROP TABLE IF EXISTS Tipo_Contribuinte;
DROP TABLE IF EXISTS Municipio;
DROP TABLE IF EXISTS UF;

CREATE TABLE UF(
  sigla VARCHAR(2),
  nome VARCHAR(20),
  id INT PRIMARY KEY NOT NULL,
  bandeira BLOB NULL
);

CREATE TABLE Municipio (
	nome VARCHAR(60),
	uf_id INT,

	CONSTRAINT fk_Municipio_UF
	FOREIGN KEY (uf_id)
	REFERENCES UF(id),

	id VARCHAR(63) NOT NULL,
	CONSTRAINT PK_Municipio PRIMARY KEY (id)
);

CREATE TABLE Tipo_Contribuinte(
  tipo VARCHAR(20), 
  id INT PRIMARY KEY NOT NULL
);

CREATE TABLE NCM_SH_Tipo (
  id INT PRIMARY KEY NOT NULL,
  tipo_de_produto VARCHAR(1023)
);

CREATE TABLE Destinatario(
  cnpj VARCHAR(20),
  nome VARCHAR(255),
  uf_id INT, 
  
  CONSTRAINT fk_UF_Destinatario 
  FOREIGN KEY (uf_id) 
  REFERENCES UF(id),
  
  fk_tipo_contribuinte INT,

  CONSTRAINT fk_Destinatario_3
  FOREIGN KEY (fk_tipo_contribuinte) 
  REFERENCES Tipo_Contribuinte(id),
  
  id VARCHAR(36) PRIMARY KEY NOT NULL
);

CREATE TABLE Emitente(
  CPF_CNPJ VARCHAR(20),
  razao_Social VARCHAR(255),
  inscricao_Estadual VARCHAR(63),
  municipio_id VARCHAR(36),
  
  CONSTRAINT fk_Emitente_Municipio
  FOREIGN KEY (municipio_id) 
  REFERENCES Municipio(id),
  
  id VARCHAR(36) PRIMARY KEY
);

CREATE TABLE Tipo_Evento(
  descricao VARCHAR(255),
  id VARCHAR(36) PRIMARY KEY NOT NULL
);

CREATE TABLE Evento(
  tipo_evento_id VARCHAR(63),
  
  CONSTRAINT fk_TipoEvento_id 
  FOREIGN KEY (tipo_evento_id) 
  REFERENCES Tipo_Evento(id),
  
  data_hora DATETIME,
  protocolo VARCHAR(255),
  motivo VARCHAR(1000),
  id VARCHAR(36) PRIMARY KEY NOT NULL
);

CREATE TABLE Presenca_Comprador(
  codigo INT,
  tipo_Presenca VARCHAR(63),
  id VARCHAR(36) PRIMARY KEY NOT NULL
);

CREATE TABLE Natureza_Operacao(
  descricao VARCHAR(255),
  id VARCHAR(36) PRIMARY KEY NOT NULL
);

CREATE TABLE Nota_Fiscal(
  chave_de_Acesso VARCHAR(255),
  serie INT,
  numero INT,
  data_Emissao DATETIME,
  data_HoraEventoMaisRecente DATETIME,

  -- Destino da operação (interno ou interestadual)
  -- true = interno; false = interestadual
  destino_interno BOOLEAN,

  valor FLOAT,
  consumidor_final BOOLEAN,
  id VARCHAR(36) PRIMARY KEY NOT NULL,

  -- venda, retorno do conserto, venda a prazo, etc
  fk_natureza_operacao_id VARCHAR(36),

  CONSTRAINT fk_Nota_Fiscal_Natureza_Operacao
  FOREIGN KEY (fk_Natureza_Operacao_id) 
  REFERENCES Natureza_Operacao (id),

  -- Eventos como Manifestação, Autorização de uso, cancelamento, etc
  fk_evento_id VARCHAR(36),

  CONSTRAINT fk_Nota_Fiscal_Evento
  FOREIGN KEY (fk_evento_id) 
  REFERENCES Evento (id),

  fk_emitente_id VARCHAR(36),

  CONSTRAINT fk_Nota_Fiscal_Emitente
  FOREIGN KEY (fk_emitente_id) 
  REFERENCES Emitente (id),

  fk_destinatario_id VARCHAR(36),

  CONSTRAINT fk_Nota_Fiscal_Destinatario
  FOREIGN KEY (fk_destinatario_id) 
  REFERENCES Destinatario (id),

  -- presencial, teleatendimento, internet, etc
  fk_presenca_comprador_id VARCHAR(36),

  CONSTRAINT fk_Nota_Fiscal_Presenca_Comprador
  FOREIGN KEY (fk_presenca_comprador_id) 
  REFERENCES Presenca_Comprador (id)
    
);

CREATE TABLE Produto(
  quantidade_unitaria INT,
  unidade VARCHAR(6),
  descricao VARCHAR(255),

  fk_codigo_NCM_SH INT,

  CONSTRAINT fk_Produto_Codigo_NCM_SH
  FOREIGN KEY (fk_codigo_NCM_SH) 
  REFERENCES NCM_SH_Tipo (id),

  CFOP VARCHAR(20),
  valor_unitario FLOAT,
  id VARCHAR(36) PRIMARY KEY NOT NULL
  
);

CREATE TABLE Produto_NotaFiscal (
  fk_Produto_id VARCHAR(63),
  
  CONSTRAINT fk_ProdutoNotaFiscal_Produto
  FOREIGN KEY (fk_Produto_id) 
  REFERENCES Produto (id),
  
  fk_Nota_Fiscal_id VARCHAR(63),
  
  CONSTRAINT fk_ProdutoNotaFiscal_Nota_Fiscal
  FOREIGN KEY (fk_Nota_Fiscal_id) 
  REFERENCES Nota_Fiscal (id),
  
  numero_produto INT,
  valor_total FLOAT,

    
  PRIMARY KEY(fk_Produto_id, fk_Nota_Fiscal_id, numero_produto)
);

CREATE TABLE Evento_NotaFiscal(
  notaFiscal_id VARCHAR(36) NOT NULL,
  
  CONSTRAINT fk_Evento_NotaFiscal_Nota_Fiscal
  FOREIGN KEY (notaFiscal_id)
  REFERENCES Nota_Fiscal (id),
  
  evento_id VARCHAR(36) NOT NULL,
  
  CONSTRAINT fk_Evento_NotaFiscal_Evento
  FOREIGN KEY (evento_id)
  REFERENCES Evento (id),
  
  PRIMARY KEY (notaFiscal_id, evento_id)
  
);
