# AnaliseTransparencia

Projeto para Disciplina de Banco de Dados do curso de Ciência da Computação da UFRJ - 2022.1

## Dataset

Escolhemos os Datasets do site do governo para analisar dados de compra e venda através do registro de notas fiscais.

## Entidades
- Compradores
- Vendedores
- Produtos


## Consultas

Critérios das consultas:
- 1 com select 
- 1 com subselect
- 2 com junção de apenas 2 tabelas 
- 2 com junção de 3+ tabelas
- 1 com operação sobre conjunto
- 3 com agregação

## Aplicação 

| Consulta                        | Aplicação        | Gráfico                                                                                                   |
| --------------------------------| -----------------|-----------------------------------------------------------------------------------------------------------|
|1 com select                     |filtro de pesquisa|                                                                                                           |
|1 com subselect                  |                  |                                                                                                           |
|2 com junção de apenas 2 tabelas |                  |quais os Produtos comprados por um Comprador ao longo do tempo                                             |
|2 com junção de 3+ tabelas       |                  |para um mesmo comprador comprando um mesmo produto ao longo do tempo, qual foi a variação de preço/vendedor|
|1 com operação sobre conjunto    |                  ||
|3 com agregação                  |                  ||
