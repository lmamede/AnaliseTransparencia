package ufrj.br.analisetransparenciabackend.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ufrj.br.analisetransparenciabackend.model.dto.*;
import ufrj.br.analisetransparenciabackend.service.AnaliseTransparenciaService;

@CrossOrigin
@RestController
@RequestMapping(path = "/transparencia")
public class AnaliseTransparenciaController {

    @Autowired
    private AnaliseTransparenciaService analiseTransparenciaService;

    @GetMapping(path = "/produtosValorMaior/{valor}")
    public List<ProdutoDTO> recuperarProdutoPorCodigoNcmSh(@PathVariable Float valor) 
    {
        return analiseTransparenciaService.recuperarProdutoPorValorMaior(valor);
    }
    
    @GetMapping(path = "/notasFicais/produto/{descricao}")
    public List<NotaFiscalDTO> recuperarNotasFiscaisPorDescricaoProduto(@PathVariable String descricao)
    {
    	return analiseTransparenciaService.recuperarNotasFiscaisPorDescricaoProduto(descricao);
    }
    
    @GetMapping(path = "/quantidadeNotasPorNaturezaOperacao")
    public List<QuantidadeNotasNaturezaoperacaoDTO> recuperarQuantidadeNotasPorNaturezaOperacao()
    {
    	return analiseTransparenciaService.recuperarQuantidadeNotasPorNaturezaOperacao();
    }
    
    @GetMapping(path = "/dezEstadosQueMaisGastaram")
    public List<TotalGastoPorUFDTO> recuperarDezUFsQueMaisGastaram() 
    {
    	return analiseTransparenciaService.recuperarDezUFsQueMaisGastaram();
    }
    
    @PostMapping(path = "/quantidadeNotasFiscaisEmitenteFezParaDestinatario")
    public List<QuantidadeNotaFiscalEmitenteDestinarioDTO> recuperarQuantasNotasFiscaisEmitenteFezParaDestinatario(@RequestBody Map<String, String> dados)
    {
    	return analiseTransparenciaService.recuperarQuantasNotasFiscaisEmitenteFezParaDestinatario(dados.get("emitente"), dados.get("destinatario"));
    }
    
    @PostMapping(path = "/emitente")
    public List<String> recuperarEmitentes(@RequestBody EmitenteDTO emitente)
    {
    	return analiseTransparenciaService.recuperarEmitentes(emitente.getEmitente());
    }
    
    @PostMapping(path = "/destinatario")
    public List<String> recuperarDestinatarios(@RequestBody DestinatarioDTO destinatario)
    {
    	return analiseTransparenciaService.recuperarDestinatario(destinatario.getDestinatario());
    }
    
    @PostMapping(path = "/valorGastoPorEmitente")
    public List<EmitenteDTO> recuperarValorGastoPorEmitente(@RequestBody EmitenteDTO emitente)
    {
    	return analiseTransparenciaService.recuperarValorGastoPorEmitente(emitente.getEmitente());
    }

    @PostMapping(path = "/recuperarNomesNaturezaOperacaoAutocomplete")
    public List<String> recuperarNomesNaturezaOperacaoAutocomplete(@RequestBody Map<String, String> busca)
    {
        return analiseTransparenciaService.recuperarNomesNaturezaOperacaoAutocomplete(busca.get("busca"));
    }

    @PostMapping(path = "/quantidadeNotasFiscaisPorNaturezaOpercao")
    public List<NaturezaOperacaoNfDTO> recuperarQuantidadeNotasFiscaisPorNaturezaOpercao(@RequestBody Map<String, String> naturezaOp)
    {
        return analiseTransparenciaService.recuperarQuantidadeNotasFiscaisPorNaturezaOpercao(naturezaOp.get("naturezaOp"));
    }
    
    @PostMapping(path = "/totalPagoPorDestinatario")
    public List<TotalPagoPorDestinatarioDTO> receuperarTotalPagoPorDestinatario(@RequestBody Map<String, String> destinatario)
    {
    	return analiseTransparenciaService.receuperarTotalPagoPorDestinatario(destinatario.get("destinatario"));
    }
    
    @PostMapping(path = "/recuperarTipoProduto")
    public List<String> recuperarTipoProdutoAutocomplete(@RequestBody Map<String, String> busca)
    {
        return analiseTransparenciaService.recuperarTipoProduto(busca.get("tipoProduto"));
    }
    
    @PostMapping(path = "/mediaGastaPorTipoProduto")
    public List<MediaGastaPorTipoProdutoDTO> recuperarMediaGastaPorTipoProduto(@RequestBody Map<String, String> busca)
    {
        return analiseTransparenciaService.recuperarMediaGastaPorTipoProduto(busca.get("tipoProduto"));
    }
    
    @GetMapping(path = "/recuperarEstados")
    public List<EstadoDTO> recuperarEstados()
    {
    	return analiseTransparenciaService.recuperarEstados();
    }
    
    @PostMapping(path = "/produtosComTotalGasto")
    public List<ProdutoDTO> recuperarProdutosComTotalGasto(@RequestBody Map<String, String> produto)
    {
    	return analiseTransparenciaService.recuperarProdutosComValorTotalGasto(produto.get("produto"));
    }
    
    @PostMapping(path = "/notasPorPeriodo")
    public List<NotaFiscalDTO> recuperarNotasPorperiodo(@RequestBody Map<String, Date> datas)
    {
    	return analiseTransparenciaService.recuperarNotasFiscaisPorPeriodo(datas.get("dataInicio"), datas.get("dataFim"));
    }

    @PostMapping(path = "/valoresEmitidosERecebidos")
    public ValoresEmitidosERecebidosDTO recuperarValoresEmitidosERecebidosPorUF(@RequestBody Map<String, String> sigla){
        return  analiseTransparenciaService.recuperarValoresEmitidosERecebidosPorUF(sigla.get("sigla"));
    }

    @GetMapping(path = "/recuperarNotaFiscalCompleta/{chave}")
    public NotaFiscalProdutoDTO recuperarNotaFiscalCompleta(@PathVariable String chave){
        return analiseTransparenciaService.recuperarNotaFiscalCompleta(chave);
    }
}

