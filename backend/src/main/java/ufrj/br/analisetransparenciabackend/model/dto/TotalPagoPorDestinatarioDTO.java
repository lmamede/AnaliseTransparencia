package ufrj.br.analisetransparenciabackend.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TotalPagoPorDestinatarioDTO {
	
	private String destinatario;
	
	private String valorPago;

}
