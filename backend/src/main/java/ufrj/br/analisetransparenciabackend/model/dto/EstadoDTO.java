package ufrj.br.analisetransparenciabackend.model.dto;

import javax.persistence.Lob;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EstadoDTO {
	
	private String sigla;
	
	private String id;
	
	@Lob
	private byte[] bandeira;
	
	private String nome;

}
