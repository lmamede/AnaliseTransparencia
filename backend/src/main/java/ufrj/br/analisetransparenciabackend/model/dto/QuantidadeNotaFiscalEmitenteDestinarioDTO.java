package ufrj.br.analisetransparenciabackend.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuantidadeNotaFiscalEmitenteDestinarioDTO {
	
	private String emitente;
	
	private String destinatario;
	
	private Integer quantidade;
}
