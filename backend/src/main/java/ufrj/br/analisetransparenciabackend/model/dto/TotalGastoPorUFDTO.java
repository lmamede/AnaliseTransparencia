package ufrj.br.analisetransparenciabackend.model.dto;

import java.sql.Blob;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TotalGastoPorUFDTO {
	
	private String sigla;
	
	private Blob bandeira;
	
	private String valorTotal;

}
