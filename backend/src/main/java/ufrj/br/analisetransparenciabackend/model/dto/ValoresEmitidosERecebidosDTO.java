package ufrj.br.analisetransparenciabackend.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValoresEmitidosERecebidosDTO {

    private float valorEmitido;
    private float valorRecebido;

}
