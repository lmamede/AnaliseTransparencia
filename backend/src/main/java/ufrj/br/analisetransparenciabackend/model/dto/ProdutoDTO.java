package ufrj.br.analisetransparenciabackend.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProdutoDTO {

	private String produto;

	private Integer quantidade;

	private String unidade;
	
	private String valorUnitario;
	
	private String valorTotal;

	private String tipoProduto;

	private String CFOP;
	
}
