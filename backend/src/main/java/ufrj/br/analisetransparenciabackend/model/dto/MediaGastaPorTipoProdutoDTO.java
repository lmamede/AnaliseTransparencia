package ufrj.br.analisetransparenciabackend.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MediaGastaPorTipoProdutoDTO {
	
	private String tipoProduto;
	
	private String media;

}
