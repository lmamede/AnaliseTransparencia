import csv
import uuid
from datetime import datetime

file = open('../CSVS Originais - Nota Fiscal/202201_NFe_NotaFiscalItem.csv', encoding='ISO-8859-1')

arquivoCSV = csv.reader(file, delimiter = ';')

headers = []
headers = next(arquivoCSV)

for i in range(len(headers)):
	print(headers[i], i)

campos_desejados = [4, -1]
natureza = []

arquivoInserts = open("./Insert_Natureza_Operacao.sql", "w")

for row in arquivoCSV:
	descricao = row[4]
	if descricao in natureza:
		continue
	
	string = "INSERT INTO Natureza_Operacao VALUES ("
	for element in campos_desejados:
		if element == -1:
			string += "\"" + str(uuid.uuid1()) + "\""
		else:
			string += "\"" + str(row[element] + "\"")
			
		if element != campos_desejados[-1]:
			string += ", "
			
	string += ");\n"
	arquivoInserts.write(string)
	natureza.append(descricao)
	
arquivoInserts.close()
