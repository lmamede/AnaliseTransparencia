import csv
import uuid
from datetime import datetime

file = open('../CSVS Originais - Nota Fiscal/202201_NFe_NotaFiscalEvento.csv', encoding='ISO-8859-1')

arquivoCSV = csv.reader(file, delimiter = ';')

headers = []
headers = next(arquivoCSV)

for i in range(len(headers)):
	print(headers[i], i)


campos_desejados = [0, 8]

arquivoInserts = open("./Insert_Evento_NotaFiscal.sql", "w")

for row in arquivoCSV:
	
	string = "INSERT INTO Evento_NotaFiscal VALUES ("
	for element in campos_desejados:
		string += "\"" + str(row[element] + "\"")
			
		if element != campos_desejados[-1]:
			string += ", "
			
	string += ");\n"
	arquivoInserts.write(string)
	
arquivoInserts.close()
