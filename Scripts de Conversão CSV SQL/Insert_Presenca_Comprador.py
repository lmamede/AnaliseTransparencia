import csv
import uuid
from datetime import datetime

file = open('202201_NFe_NotaFiscal.csv', encoding='ISO-8859-1')

arquivoCSV = csv.reader(file, delimiter = ';')

headers = []
headers = next(arquivoCSV)

campos = ["chave_de_Acesso",
    "serie",
    "numero",
    "data_Emissao",
    "data_HoraEventoMaisRecente",
    "destino_interno",
    "valor",
    "consumidor_final",
    "id",
    "fk_natureza_operacao_id",
    "fk_evento_id",    
    "fk_emitente_id",
    "fk_destinatario_id",
    "fk_presenca_comprador_id" ]

lista_presencas = ['0 - NÃO SE APLICA', '9 - OPERAÇÃO NÃO PRESENCIAL, OUTROS', '1 - OPERAÇÃO PRESENCIAL', '5 - NÃO INFORMADO', '2 - OPERAÇÃO NÃO PRESENCIAL, PELA INTERNET', '3 - OPERAÇÃO NÃO PRESENCIAL, TELEATENDIMENTO']

arquivoInserts = open("./Insert_Presenca_Comprador.sql", "w")

_id = 1
for elemento in lista_presencas:
    codigo, descricao = elemento.split(" - ")
    
    arquivoInserts.write("INSERT INTO Presenca_Comprador VALUES ({0}, \'{1}\', {2});\n"
                            .format(codigo, descricao, _id))

    _id += 1

arquivoInserts.close()
    
	


	
	
	
	
	
	
	
	
	
	
	
	
	
	


