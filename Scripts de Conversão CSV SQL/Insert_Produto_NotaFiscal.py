import csv
import uuid
from datetime import datetime

file = open('../CSVS Originais - Nota Fiscal/202201_NFe_NotaFiscalItem.csv', encoding='ISO-8859-1')

arquivoCSV = csv.reader(file, delimiter = ';')

headers = []
headers = next(arquivoCSV)

for i in range(len(headers)):
	print(headers[i], i)

campos_desejados = [19, 0, 18, 26]
numeros = [18, 26]

arquivoInserts = open("./Insert_ProdutoNotaFiscal.sql", "w")

for row in arquivoCSV:
	
	string = "INSERT INTO ProdutoNotaFiscal VALUES ("
	for element in campos_desejados:
		if element in numeros:
			string += str(row[element].replace(",", ".")) + ""
		else:
			string += "\"" + str(row[element] + "\"")
			
		if element != campos_desejados[-1]:
			string += ", "
			
	string += ");\n"
	arquivoInserts.write(string)
	
arquivoInserts.close()
